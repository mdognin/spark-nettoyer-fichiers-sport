# Projet perso pour manipuler des JSON issues de mes activités sportives

- preprocessing : Le premier script python permet de préparer les données. C’est-à-dire qu'on va seulement garder les données utile. Le reste sera supprimé 

- suppressions : Dans le second script on nettoie les données des timeline. Les timeline sont les données qui sont enregistrées par les devices à intervalles de temps régulier. On estime que si la valeur enregistrée à T1 est la même que celle enregistrée à T0 on va pouvoir supprimer la valeur de T1puisqu'il n'y a pas de changement 

- convert_json : Dans mon exemple j'ai créé un script pour stocker les information contenues dans les fichier JSON dans des fichiers dits "parquet".
